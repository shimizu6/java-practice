package rpg02;

/**
 * PlayGameクラス
 */
public class PlayGame {

	public static void main(String[] args) {

		// プレイヤー生成
		Charactor player = new Charactor("勇者リナクス", 20, 5);
		// プレイヤーの情報を表示
		player.showInfo();

		// 敵キャラクター生成
		Charactor enemy = new Charactor("スライム", 7, 3);
		// 敵の情報を表示
		enemy.showInfo();
	}
}
