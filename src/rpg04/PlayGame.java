package rpg04;

/**
 * PlayGameクラス
 */
public class PlayGame {

	public static void main(String[] args) {

		// プレイヤー生成
		Charactor player = new Charactor("勇者リナクス", 20, 5);

		// 敵キャラクター生成
		Charactor enemy = new Charactor("スライム", 7, 3);

		// ゲームスタート
		System.out.println("★★★ ゲーム開始 ★★★");

		// 1回目の攻撃
		player.attack(enemy);

		// 2回目の攻撃
		player.attack(enemy);

		// 3回目の攻撃
		player.attack(enemy);

		System.out.println("★★★ ゲーム終了 ★★★");

	}
}
