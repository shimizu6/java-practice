package rpg06;
/**
 * キャラクタークラス
 */
public class Charactor {
	// フィールド
	private String name; // 名前
	private int lifePoint; // ライフポイント
	private int attackPower; // 攻撃力

	// コンストラクタ
	public Charactor() {
	}
	public Charactor(String name, int lifePoint, int attackPower) {
		this.name = name;
		this.lifePoint = lifePoint;
		this.attackPower = attackPower;
		// キャラクター情報の表示
		showInfo();
	}
	// getter/setter
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getLifePoint() {
		return lifePoint;
	}
	public void setLifePoint(int lifePoint) {
		// 値がマイナスならば0でライフポイントを更新する
		if (lifePoint < 0) {
			lifePoint = 0;
		}
		this.lifePoint = lifePoint;
	}
	public int getAttackPower() {
		return attackPower;
	}
	public void setAttackPower(int attackPower) {
		this.attackPower = attackPower;
	}

	/** 情報表示 */
	public void showInfo() {
		System.out.println(name + "（HP:" + lifePoint + "/攻撃力:" + attackPower + "）");
	}

	/** 攻撃メソッド */
	public void attack(Charactor target) {

		System.out.println(name + "の攻撃");

		// 相手のライフポイントを取得
		int targetLifePoint = target.getLifePoint();
		// ライフポイントを減らす
		targetLifePoint -= attackPower;
		// 相手のライフポイントを更新
		target.setLifePoint(targetLifePoint);

		// 相手の情報を表示
		target.showInfo();
	}
}
