package rpg08;

/**
 * PlayGameクラス
 */
public class PlayGame {

	public static void main(String[] args) {

		// プレイヤー生成
		Charactor player = new Charactor("勇者リナクス", 20, 5);

		// 敵キャラクター生成
		Charactor enemy = new Charactor("スライム", 7, 3);

		// ゲームスタート
		System.out.println("★★★ ゲーム開始 ★★★");

		// 敵のライフポイントが0より大きい間
		while (enemy.getLifePoint() > 0) {
			player.attack(enemy);
			enemy.attack(player);
		}

		System.out.println("★★★ ゲーム終了 ★★★");

	}
}
