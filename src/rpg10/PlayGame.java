package rpg10;

/**
 * PlayGameクラス
 */
public class PlayGame {

	public static void main(String[] args) {

		// プレイヤー生成
		Charactor player = new Charactor("勇者リナクス", 20, 5);

		// 敵キャラクター生成
		Charactor enemy = new Charactor("スライム", 7, 3);

		// ゲームスタート
		System.out.println("★★★ ゲーム開始 ★★★");

		// 戦闘呼び出し
		if(!battle(player, enemy)) {
			System.out.println("ゲームオーバー");
		}

		System.out.println("★★★ ゲーム終了 ★★★");

	}

	/**
	 * 戦闘メソッド
	 * @param player プレイヤー
	 * @param target 戦う対象
	 * @return true 勝利 / false 敗北
	 */
	public static boolean battle(Charactor player, Charactor target) {

		System.out.println(target.getName() + "があらわれた！");

		// 勝利フラグ
		boolean isWon = false;

		// どちらかが倒れるまで攻撃を続ける
		while (true) {
			// プレイヤーの攻撃
			if (player.attack(target)) {
				// プレイヤーが勝利した場合
				isWon = true; // 勝利フラグを立てる
				System.out.println(target.getName() + "とのたたかいに勝利した！");
				break;
			}
			// 敵の攻撃
			if (target.attack(player)) {
				// 敵が勝利した場合
				System.out.println(target.getName() + "とのたたかいに負けてしまった...");
				break;
			}
		}

		return isWon;
	}
}
