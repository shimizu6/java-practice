package rpg17;

import java.util.Scanner;

/**
 * キャラクタークラス
 */
public class Charactor {
	// フィールド
	private String name; // 名前
	private int lifePoint; // ライフポイント
	private int maxLifePoint; // 最大ライフポイント
	private int attackPower; // 攻撃力

	// コンストラクタ
	public Charactor() {
	}
	public Charactor(String name, int lifePoint, int attackPower) {
		this.name = name;
		this.lifePoint = lifePoint;
		this.maxLifePoint = lifePoint;
		this.attackPower = attackPower;
		// キャラクター情報の表示
		showInfo();
	}
	// getter/setter
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getLifePoint() {
		return lifePoint;
	}
	public void setLifePoint(int lifePoint) {
		// 値がマイナスならば0でライフポイントを更新する
		if (lifePoint < 0) {
			lifePoint = 0;
		}
		this.lifePoint = lifePoint;
	}
	public int getAttackPower() {
		return attackPower;
	}
	public void setAttackPower(int attackPower) {
		this.attackPower = attackPower;
	}

	/** 情報表示 */
	public void showInfo() {
		System.out.println(name + "（HP:" + lifePoint + "/攻撃力:" + attackPower + "）");
	}

	/** 行動選択メソッド(true:攻撃 false:回復) */
	public boolean selectAction() {
		try {
			System.out.print("行動を選択して下さい（1:攻撃 2:回復）：");

			// キーボードからの入力
			Scanner scan = new Scanner(System.in);
			int input = scan.nextInt();
			scan.close();

			// 2(回復)以外の場合はすべて攻撃とする
			if (input == 2) {
				// 回復
				return false;
			} else {
				// 攻撃
				return true;
			}
		} catch (Exception e) {
			// エラーが発生した場合は1を返却する
			return true;
		}

	}

	/** 攻撃メソッド */
	public boolean attack(Charactor target) {

		System.out.println(name + "の攻撃");

		// 相手のライフポイントを取得
		int targetLifePoint = target.getLifePoint();
		// ライフポイントを減らす
		targetLifePoint -= attackPower;
		// 相手のライフポイントを更新
		target.setLifePoint(targetLifePoint);

		// 相手の情報を表示
		target.showInfo();

		// 相手を倒した場合trueを返却
		if (target.getLifePoint() <= 0) {
			return true;
		} else {
			return false;
		}
	}

	/** 回復メソッド */
	public void heal() {
		// 最大ライフポイントの半分を回復
		lifePoint += (int) (maxLifePoint / 2);
	}
}
