package rpg17;

import java.util.Random;

/**
 * モンスタークラス
 */
public class Monster extends Charactor {
	// コンストラクタ
	public Monster() {
		super();
	}
	public Monster(String name, int lifePoint, int attackPower) {
		super(name, lifePoint, attackPower);
	}

	/** 行動選択メソッド(true:攻撃 false:回復) */
	public boolean selectAction() {
		// 0～9のランダムな整数を作成
		Random rand = new Random();
		int act = rand.nextInt(10);
		if (act == 0) {
			// 回復
			return false;

		} else {
			// 攻撃
			return true;
		}
	}

}
