package rpg17;

import java.util.Scanner;

/**
 * PlayGameクラス
 */
public class PlayGame {

	public static void main(String[] args) {

		// プレイヤー生成
		Charactor player = new Player("勇者リナクス", 20, 5);

		// 敵キャラクター生成
		Charactor[] enemys = {
				new Monster("スライム", 7, 3),
				new Monster("ソンビ", 10, 3),
				new Monster("ゴースト", 12, 4)
				};

		// ゲームスタート
		System.out.println("★★★ ゲーム開始 ★★★");

		// すべての敵とたたかう
		for (Charactor enemy : enemys) {
			// 戦闘呼び出し
			if(!battle(player, enemy)) {
				System.out.println("ゲームオーバー");
				break;
			}
		}


		System.out.println("★★★ ゲーム終了 ★★★");

	}

	/**
	 * 戦闘メソッド
	 * @param player プレイヤー
	 * @param target 戦う対象
	 * @return true 勝利 / false 敗北
	 */
	public static boolean battle(Charactor player, Charactor target) {

		System.out.println(target.getName() + "があらわれた！");

		// 勝利フラグ
		boolean isWon = false;

		// キーボードからの入力準備
		Scanner scan = new Scanner(System.in);

		// どちらかが倒れるまで攻撃を続ける
		while (true) {


			// プレイヤーの行動選択
			if (player.selectAction()) {
				// 攻撃
				if (player.attack(target)) {
					// プレイヤーが勝利した場合
					isWon = true; // 勝利フラグを立てる
					System.out.println(target.getName() + "とのたたかいに勝利した！");
					break;
				}

			} else {
				// 回復
				player.heal();
			}

			// 敵の行動選択
			if (target.selectAction()) {
				// 攻撃
				if (target.attack(player)) {
					// 敵が勝利した場合
					System.out.println(target.getName() + "とのたたかいに負けてしまった...");
					break;
				}
			} else {
				// 回復
				target.heal();
			}

		}

		// 改行
		System.out.println();

		return isWon;
	}
}
