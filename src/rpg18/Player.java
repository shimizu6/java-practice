package rpg18;

/**
 * プレイヤークラス
 */
public class Player extends Charactor {

	// フィールド
	private Weapon weapon; // 武器

	// コンストラクタ
	public Player() {
		super();
	}
	public Player(String name, int lifePoint, int attackPower) {
		super(name, lifePoint, attackPower);
	}
	public Player(String name, int lifePoint, int attackPower, Weapon weapon) {
		super(name, lifePoint, attackPower);
		this.weapon = weapon;
	}
	// Getter/Setter
	public Weapon getWeapon() {
		return weapon;
	}
	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}
	// オーバーライド
	public int getAttackPower() {
		return super.getAttackPower() + weapon.getAddAttackPower();
	}
}
