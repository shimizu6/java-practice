package rpg18;

/**
 * 武器クラス
 */
public class Weapon {
	// フィールド
	private String name; // 名前
	private int addAttackPower; // 追加攻撃力

	// コンストラクタ
	public Weapon() {
	}
	public Weapon(String name, int addAttackPower) {
		this.name = name;
		this.addAttackPower = addAttackPower;
	}

	// Getter/Setter
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAddAttackPower() {
		return addAttackPower;
	}
	public void setAddAttackPower(int addAttackPower) {
		this.addAttackPower = addAttackPower;
	}

}
